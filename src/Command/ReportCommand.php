<?php
/**
 * Author: gellu
 * Date: 10.04.2016 10:53
 */

namespace Command;

use Service\ScoreboardService;
use Service\UserService;

class ReportCommand extends Command
{
	public function report()
	{
		/** @var ScoreboardService $scoreboardService */
		$scoreboardService = $this->container->scoreboard;

		file_put_contents('data/report.csv', "name\trealisation\tresults\tvalues\tsurvey_count\thas_boss_feedback\n");

		foreach($this->db->user as $user)
		{
			$results = $scoreboardService->getResults(1, $user['id']);
			$values = $scoreboardService->getValues(1, $user['id']);

			$line = $user['name'] .' '. $user['surname'] ."\t".
					$results['realisation'] ."\t".
					$results['score'] ."\t".
					$values['score'] ."\t".
					$scoreboardService->getSurveyCount(1, $user['id']) ."\t".
					$results['has_boss']."\n";

			file_put_contents('data/report.csv', $line, FILE_APPEND);

		}
	}

	public function reportLinks($talentMatrixId)
	{
		/** @var UserService $userService */
		$userService = $this->container->user;
        /** @var ScoreboardService $scoreboardService */
        $scoreboardService = $this->container->scoreboard;

        $userTalenMatrixs = $this->db
            ->user_talentmatrix
            ->where('talentmatrix_id', $talentMatrixId);
//            ->group('user_id');

		foreach ($userTalenMatrixs as $userTalentmatrix)
		{
            if (0 === $scoreboardService->getSurveyCount($talentMatrixId, $userTalentmatrix['user_id']))
            {
                continue;
            }

			$user = $this->db->user->where('id', $userTalentmatrix['user_id'])->fetch();

			$userHash = $userService->getUserHashFromId($user['id']);
			echo $user['name'] . " " . $user['surname'] . "\t" . $user['email'] . "\t" . 'http://talentmatrix.docplanner.io/' . $talentMatrixId . '/' . $userHash . "\n";
		}
	}
}