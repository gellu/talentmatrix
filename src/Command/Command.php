<?php
/**
 * Author: gellu
 * Date: 10.04.2016 10:58
 */

namespace Command;

use Pimple\Container;

class Command
{
	/** @var  Container */
	protected $container;

	/** @var  \NotORM */
	protected $db;

	public function __construct(Container $container)
	{
		$this->container 	= $container;
		$this->db			= $container->get('db');
	}

}