<?php
/**
 * Author: gellu
 * Date: 12.04.2016 14:44
 */

namespace Command;

use Service\Matrix;
use Service\ScoreboardService;

class CalculateScoreboardCommand extends Command
{
	public function run($type, $talentmatrixId)
	{
		switch($type)
		{
			case Matrix::TALENT_MATRIX:
				$this->calculateTalentMatrix($talentmatrixId);
				break;
			case Matrix::LEADER_MATRIX;
				$this->calculateLeaderMatrix($talentmatrixId);
				break;
		}
	}

	public function calculateLeaderMatrix($talentmatrixId)
	{
		/** @var ScoreboardService $scoreboardService */
		$scoreboardService = $this->container->scoreboard;

		foreach ($this->db->user as $user)
		{
			if ($scoreboardService->hasScoreboard($talentmatrixId, $user['id']) && !$scoreboardService->hasStoredScoreboard($talentmatrixId, $user['id']))
			{
				$scores = $scoreboardService->getScores($talentmatrixId, $user['id']);

				if (0 === count($scores))
				{
					continue;
				}

				foreach ($scores as $surveyQuestionId => $score)
				{
					$this->db->user_talentmatrix->insert([
						'talentmatrix_id'    => $talentmatrixId,
						'user_id'            => $user['id'],
						'score'              => $score,
						'survey_question_id' => $surveyQuestionId
					]);
				}
			}

		}
	}

	public function calculateTalentMatrix($talentmatrixId)
	{
		/** @var ScoreboardService $scoreboardService */
		$scoreboardService = $this->container->scoreboard;

		foreach($this->db->user as $user)
		{
			if($scoreboardService->hasScoreboard($talentmatrixId, $user['id']) && !$scoreboardService->hasStoredScoreboard($talentmatrixId, $user['id']))
			{
				$results = $scoreboardService->getResults($talentmatrixId, $user['id']);
				$values = $scoreboardService->getValues($talentmatrixId, $user['id']);
				$this->db->user_talentmatrix->insert([
					'talentmatrix_id'		=> $talentmatrixId,
					'user_id'				=> $user['id'],
					'talentmatrix_results'	=> $results['score'],
					'talentmatrix_values'	=> $values['score']
				]);
			}

		}
	}
}