<?php
/**
 * Author: gellu
 * Date: 09.04.2016 19:23
 */

namespace Command;

class FeedbackImporter
{
	/** @var \NotORM */
	protected $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function import($talentMatrixId, $csvFile)
	{
	    $notFoundUsers = [];
		foreach ($this->getCSV($csvFile) as $row)
        {
            $userId = $this->getUser($row['name']);
            if (!$userId) {
                $notFoundUsers[$row['name']] = $row['name'];
            }

            $surveyId 	= $this->getSurvey($talentMatrixId);

			$rowPointer = 1;
			foreach($this->db->survey_question->where('talentmatrix_id', $talentMatrixId) as $surveyQuestion)
			{
				$this->db->survey_answer->insert([
					'survey_id'				=> $surveyId,
					'user_id'				=> $userId,
					'survey_question_id'	=> $surveyQuestion['id'],
					'answer'				=> $row[$surveyQuestion['question_type']],
					'signature'				=> $row['signature'],
				]);
				$rowPointer++;
			}
		}


		foreach ($notFoundUsers as $notFoundUser)
        {
            echo 'User not found: '. $notFoundUser ."\n";
        }
	}

	private function getUser($userName)
	{
		$userData = explode(" ", $userName, 2);
		$user = $this->db->user->where(['name' => $userData['0'], 'surname' => $userData[1]])->fetch();
		if(!$user)
        {
            $this->db->user->insert(['name' => $userData['0'], 'surname' => $userData[1]]);
            return $this->db->user->insert_id();
		}

		return $user['id'];

	}

	private function getSurvey($talentMatrixId)
	{
		$this->db->survey->insert([
			'talentmatrix_id'	=> $talentMatrixId,
			'completed_at'		=> new \NotORM_Literal("NOW()")
		]);

		return $this->db->survey->insert_id();
	}

    private function getCSV($csvFile)
    {
        $handle = fopen($csvFile, 'r');
        $header = fgetcsv($handle, null, ",");

        while ($line = fgetcsv($handle, null, ","))
        {
            $line = array_combine($header, $line);

            yield $line;
        }
    }

}
