<?php

namespace Command;

use League\Csv\Reader;

class RealisationImporter
{
	/** @var \NotORM */
	protected $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	/**
	 * @param $talentMatrixId
	 */
	public function import($talentMatrixId)
	{
		$csv = Reader::createFromPath('data/realisation.csv');
		$csv->setDelimiter("\t");
		$insert = $csv->each(function ($row) use (&$sth, $talentMatrixId) {

			$userId = $this->getUser($row[0]);
			if(!$userId)
			{
				$userId = $this->createUser(explode(" ", $row[0]));
			}

			if (!empty($row[1]))
			{
				$userManagerId 	= $this->getUserManager(explode(" ", $row[1]));
				$this->updateUser(['id' => $userId , 'manager_user_id' => $userManagerId]);
			}

			if (!empty($row[2]))
			{
				$this->updateUser(['id' => $userId , 'user_department_id' => $this->db->user_department->where('name', $row[2])->fetch('id')]);
			}

			$this->setRealisation($userId, $talentMatrixId, (float) str_replace(',', '.', $row[5]));

			return true;
		});

	}

	private function getUserManager($userParam)
	{
		$user = $this->db->user->where(['name' => $userParam[1], 'surname' => $userParam[0]])->fetch();
		if($user)
		{
			return $user['id'];
		}
		else {
			return $this->createUser([0 => $userParam[1], 1 => $userParam[0]]);
		}

	}

	private function getUser($userName)
	{
		$userData = explode(" ", $userName);
		$user = $this->db->user->where(['name' => $userData['0'], 'surname' => $userData[1]])->fetch();

		return isset($user['id']) ? $user['id'] : null;

	}


	private function createUser($userData)
	{
		$user = $this->db->user->where(['name' => $userData[0], 'surname' => $userData[1]])->fetch();
		if($user)
		{
			return $user['id'];
		}

		$this->db->user->insert(['name' => $userData[0], 'surname' => $userData[1]]);

		return $this->db->user->insert_id();
	}

	private function updateUser($userData)
	{
		if(!$userData['id'])
		{
			throw new \Exception('User id must be specified');
		}

		foreach($userData as $k => $v)
		{
			$this->db->user->insert_update(['id' => $userData['id']], [$k => $v]);
		}
	}

	private function setRealisation($userId, $talentMatrixId, $realisation)
	{
		$this->db->realisation->insert(['user_id' => $userId, 'talentmatrix_id' => $talentMatrixId, 'realisation' => $realisation]);
	}

}