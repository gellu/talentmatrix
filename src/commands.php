<?php
/**
 * Author: Grzesiek
 * Date: 04.01.2016 14:23
 */

use Service\Matrix;

$app->get('/import-realisation', function ($request, $response, $args) {

	if(!$request->getParam('talent-matrix-id'))
	{
		throw new \Exception('No talent matrix selected. Usage: import-realisation talent-matrix-id=[id]');
	}

	$dataConverter = new Command\RealisationImporter($this->db);
	$dataConverter->import((int) $request->getParam('talent-matrix-id'));
});

$app->get('/import-feedback', function ($request, $response, $args) {
	/** @var \Slim\Http\Request $request */
//	throw new \Exception('Deprecated');

	if(!$request->getParam('talent-matrix-id')) {
        throw new \Exception('No talent matrix selected. Usage: import-realisation talent-matrix-id=[id]');
    }

    if (!$request->getParam('csv-file')) {
        throw new \Exception('No csv file provided.');
    }

    $dataConverter = new Command\FeedbackImporter($this->db);
	$dataConverter->import((int) $request->getParam('talent-matrix-id'), $request->getParam('csv-file'));
});

$app->get('/report', function ($request, $response, $args) {

	$dataConverter = new Command\ReportCommand($this);
	$dataConverter->report();
});
$app->get('/report-links', function ($request, $response, $args) {
    /** @var \Slim\Http\Request $request */
    $talentMatrixId = $request->getParam('id');

    if (!$talentMatrixId)
    {
        throw new \Exception('No TalentMatrix selected');
    }

	(new Command\ReportCommand($this))->reportLinks($talentMatrixId);
});

$app->get('/calculate-leader-matrix/{talentmatrixId}', function ($request, $response, $args) {
	(new Command\CalculateScoreboardCommand($this))->run(Matrix::LEADER_MATRIX, $args['talentmatrixId']);
});

$app->get('/calculate-talent-matrix/{talentmatrixId}', function ($request, $response, $args) {
	(new Command\CalculateScoreboardCommand($this))->run(Matrix::TALENT_MATRIX, $args['talentmatrixId']);
});