<?php
// Routes

$app->get('/{talentmatrix_id}/{user_hash}', function ($request, $response, $args)
{

	/** @var \Slim\Http\Request $request */
	/** @var \Service\UserService $user */
	$user = $this->user;
	/** @var \Service\ScoreboardService $scoreboard */
	$scoreboard = $this->scoreboard;

	$talentmatrixId = (int) $args['talentmatrix_id'];
	$userId         = $user->getUserIdFromHash($args['user_hash']);

	if (!$scoreboard->hasScoreboard($talentmatrixId, $userId))
	{
//		throw new \Exception('Sorry no talentmatrix for you :(');
	}

	$talentmatrix = $scoreboard->getTalentmatrix($talentmatrixId);

	/** @var \Service\Matrix $matrixService */
	$matrixService = $this->get('matrix');
	$matrix        = $matrixService->getMatrix($talentmatrix['type']);
	$matrixResults = $matrix->getResults($talentmatrixId, $userId);

	$userTalentmatrixes = $scoreboard->getAllTalentMatrixForUser($userId);

	$params = [
		'user'               => $this->db->user->where('id', $userId)->fetch(),
		'url'                => '/' . $talentmatrixId . '/' . $args['user_hash'],
		'talentmatrix'       => $talentmatrix,
		'userTalentmatrixes' => $userTalentmatrixes,
	];

	$params = array_merge($params, $matrixResults);

	return $this->renderer->render($response, 'scoreboard/index.html.twig', $params);
});

$app->get('/{talentmatrix_id}/{user_hash}/about', function ($request, $response, $args)
{
	$talentmatrixId = (int) $args['talentmatrix_id'];
	/** @var \Service\ScoreboardService $scoreboard */
	$scoreboard = $this->scoreboard;
	/** @var \Service\UserService $user */
	$user = $this->user;

	$talentmatrix       = $scoreboard->getTalentmatrix($talentmatrixId);
	$userId             = $user->getUserIdFromHash($args['user_hash']);
	$userTalentmatrixes = $scoreboard->getAllTalentMatrixForUser($userId);

	$params = [
		'url'                => '/' . $talentmatrixId . '/' . $args['user_hash'],
		'talentmatrix'       => $talentmatrix,
		'userTalentmatrixes' => $userTalentmatrixes,
	];

	return $this->renderer->render($response, 'infopage/about.'. $talentmatrix['type'] .'.html.twig', $params);
});