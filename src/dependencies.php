<?php
// DIC configuration
use Slim\Container;

/** @var Container $container */
$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\Twig($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

$container['db'] = function ($c) {
    $pdo = new PDO(
        'mysql:dbname='. $c->get('settings')['db']['name'] . ';host='. $c->get('settings')['db']['host'].($c->get('settings')['db']['port'] ? ':'.$c->get('settings')['db']['port'] : ''),
        $c->get('settings')['db']['user'],
        $c->get('settings')['db']['pass']);

    $pdo->exec("SET CHARACTER SET utf8");
    $pdo->exec("SET NAMES utf8");
    $db = new NotORM($pdo);
    return $db;
};

$container['errorHandler'] = function($c) {
	return function ($request, $response, $exception) use ($c) {
		$c->logger->error($exception);
		var_dump($exception);
		return $c->renderer->render($response, 'base/500.html.twig', []);
	};
};
$container['notFoundHandler'] = function($c) {
	return function ($request, $response) use ($c) {
		return $c->renderer->render($response, 'base/404.html.twig', []);
	};
};

$container['scoreboard'] = function($c) {
    return new \Service\ScoreboardService($c);
};

$container['user'] = function($c) {
    return new \Service\UserService($c);
};

$container['matrix'] = function ($c) {
	return new \Service\Matrix($c);
};

$container['talentMatrix'] = function ($c) {
	return new \Service\TalentMatrix($c['scoreboard']);
};

$container['leaderMatrix'] = function ($c) {
	return new \Service\LeaderMatrix($c['scoreboard']);
};

$container['managerMatrix'] = function ($c) {
    return new \Service\ManagerMatrix($c['scoreboard']);
};
