<?php
/**
 * Author: gellu
 * Date: 09.04.2016 21:14
 */

namespace Service;

use Pimple\Container;

class ScoreboardService
{
	const SCOREBOARD_FEEDBACK_TYPE_STOP 		= 'stop';
	const SCOREBOARD_FEEDBACK_TYPE_CONTINUE 	= 'continue';

	const MINIMUM_SURVEYS_FOR_SCOREBOARD = 1;

	const SCOREBOARD_PRECISION = 2;

	/** @var \NotORM */
	private $db;

	/** @var Container */
	private $container;

	public function __construct($container)
	{
		$this->container 	= $container;
		$this->db 			= $this->container->get('db');
	}

	public function getFeedback($talentmatrixId, $userId, $type)
	{
		$feedback = [];
		$surveyQuestions = $this->db->survey_question->where(['talentmatrix_id' => $talentmatrixId, 'question_type' => $type]);
		foreach ($surveyQuestions as $surveyQuestion)
		{
			$surveyAnswers = $this->db->survey_answer->where([
					'user_id' 					=> $userId,
					'survey_question_id' 		=> $surveyQuestion['id'],
					'survey.talentmatrix.id'	=> $talentmatrixId,
			]);

			foreach($surveyAnswers as $surveyAnswer)
			{
			    if (empty($surveyAnswer['answer']))
                {
                    continue;
                }

				$feedback[] = [
				    'comment' => $surveyAnswer['answer'],
                    'signature' => $surveyAnswer['signature'],
                ];
			}
		}

		return $feedback;

	}

	public function getResults($talentmatrixId, $userId)
	{
		return $this->countResults($talentmatrixId, $userId, true);
	}

	public function getResultsMedian($talentmatrixId)
	{
		return round($this->countQuartile($talentmatrixId, 'results', 0.5), \Service\ScoreboardService::SCOREBOARD_PRECISION, PHP_ROUND_HALF_UP);
	}
	public function getResultsQuartile75($talentmatrixId)
	{
		return round($this->countQuartile($talentmatrixId, 'results', 0.75), \Service\ScoreboardService::SCOREBOARD_PRECISION, PHP_ROUND_HALF_UP);
	}

	public function getValues($talentmatrixId, $userId)
	{
		return $this->countResults($talentmatrixId, $userId, false);
	}

	public function getValuesMedian($talentmatrixId)
	{
		return round($this->countQuartile($talentmatrixId, 'values', 0.5), \Service\ScoreboardService::SCOREBOARD_PRECISION, PHP_ROUND_HALF_UP);
	}

	public function getValuesQuartile75($talentmatrixId)
	{
		return round($this->countQuartile($talentmatrixId, 'values', 0.75), \Service\ScoreboardService::SCOREBOARD_PRECISION, PHP_ROUND_HALF_UP);
	}

	private function countResults($talentmatrixId, $userId, $withRealisation)
	{
		$surveyQuestions = $this->db->survey_question
			->where('question_type', $withRealisation ? 'results' : 'values')
			->where('talentmatrix_id', $talentmatrixId);

		$results = [
			'boss' => [],
			'not-boss' => [],
		];
		foreach($surveyQuestions as $surveyQuestion)
		{

			$surveyAnswers = $this->db->survey_answer->where([
					'user_id'                => $userId,
					'survey_question_id'     => $surveyQuestion['id'],
					'survey.talentmatrix.id' => $talentmatrixId,
			]);

			foreach ($surveyAnswers as $surveyAnswer)
			{
				if ($this->isBossSurvey($surveyAnswer['survey_id'], $talentmatrixId))
				{
					$results['boss'][] = $surveyAnswer['answer'];
				}
				else
				{
					$results['not-boss'][] = $surveyAnswer['answer'];
				}

			}

		}

		$ret = ['score' => 0, 'has_boss' => count($results['boss']) > 0 ? true : false];
		if($withRealisation)
		{
			$realisation = $this->db->realisation
					->where(['talentmatrix_id' => $talentmatrixId, 'user_id' => $userId])
					->fetch('realisation');
			$ret['realisation'] = $realisation;
		}

		# there are no survey results for user
		if(count($results) == 0)
		{
			return $ret;
		}

		$countResultsBoss	 = count($results['boss']);
		$countResultsNotBoss = count($results['not-boss']);

		if($countResultsBoss > 0 && $countResultsNotBoss > 0)
		{
			$results['total-not-boss'] 	= array_sum($results['not-boss'])/count($results['not-boss']);
			$results['total-boss'] 		= array_sum($results['boss']) / count($results['boss']);
			if($withRealisation && $realisation)
			{
				$ret['score'] = $results['total-not-boss'] / 4 + $results['total-boss'] / 4 + $realisation / 2;
			}
			else {
				$ret['score'] = $results['total-not-boss'] / 2 + $results['total-boss'] / 2;
			}
		}
		elseif ($countResultsBoss > 0 && $countResultsNotBoss == 0)
		{
			$results['total-boss'] 		= array_sum($results['boss']) / count($results['boss']);
			if($withRealisation && $realisation)
			{
				$ret['score'] = $results['total-boss'] / 2 + $realisation / 2;
			}
			else {
				$ret['score'] = $results['total-boss'];
			}
		}
		elseif ($countResultsBoss == 0 && $countResultsNotBoss > 0)
		{
			$results['total-not-boss'] 	= array_sum($results['not-boss'])/count($results['not-boss']);
			if($withRealisation && $realisation)
			{
				$ret['score'] = $results['total-not-boss'] / 2 + $realisation / 2;
			} else {
				$ret['score'] = $results['total-not-boss'];
			}

		}

		return $ret;

	}

	public function countQuartile($talentmatrixId, $type, $quartile)
	{
		$res = $this->db->user_talentmatrix->where('talentmatrix_id', $talentmatrixId);
		$array = [];

		foreach($res as $r)
		{
			$array[] = $r['talentmatrix_'. $type];
		}

		sort($array);

		$pos = (count($array) - 1) * $quartile;

		$base = floor($pos);
		$rest = $pos - $base;

		if( isset($array[$base+1]) ) {
			return $array[$base] + $rest * ($array[$base+1] - $array[$base]);
		} else {
			return $array[$base];
		}
	}

	public function countQuartile2($array, $quartile)
	{
		sort($array);

		$pos = (count($array) - 1) * $quartile;

		$base = floor($pos);
		$rest = $pos - $base;

		if( isset($array[$base+1]) ) {
			return $array[$base] + $rest * ($array[$base+1] - $array[$base]);
		} else {
			return $array[$base];
		}
	}


	public function countQuartiles($talentmatrixId, $quartile)
	{
		$res = $this->db->user_talentmatrix->where('talentmatrix_id', $talentmatrixId);
		$array = [];

		foreach($res as $r)
		{
			$array[$r['survey_question_id']][] = $r['score'];
		}

		$results = [];
		foreach ($array as $id => $a)
		{
			$results[$id] = $this->countQuartile2($a, $quartile);
		}

		return $results;
	}

	public function getAllScoreboards($talentmatrixId)
	{
		$talentmatrixs = $this->db->user_talentmatrix
				->where('talentmatrix_id', $talentmatrixId);

		$results = [];
		foreach ($talentmatrixs as $talentmatrix)
		{
			$results[] = [
				'talentmatrix_values'	=> $talentmatrix['talentmatrix_values'],
				'talentmatrix_results'	=> $talentmatrix['talentmatrix_results'],
			];
		}

		return $results;
	}

	public function getLeaderMatrixAllScoreboards($talentmatrixId)
	{
		$talentmatrixs = $this->db->user_talentmatrix
			->where('talentmatrix_id', $talentmatrixId);

		$results = [];
		foreach ($talentmatrixs as $talentmatrix)
		{
			$results[$talentmatrix['survey_question_id']][] = $talentmatrix['score'];
		}

		return $results;
	}

    /**
     * @param int   $talentmatrixId
     * @param array $typeNames
     *
     * @return array
     */
	public function getAllScoreboardsGrouppedByUser($talentmatrixId, $typeNames = ['values', 'results'])
    {
        $surveyQuestionQuery = $this->db->survey_question
            ->where('talentmatrix_id', $talentmatrixId)
            ->where('question_type', $typeNames);

        $surveyQuestions = [];

        foreach ($surveyQuestionQuery as $surveyQuestion)
        {
            $surveyQuestions[$surveyQuestion['id']] = $surveyQuestion['question_type'];
        }

        $talentmatrixQuery = $this->db->user_talentmatrix
            ->where('talentmatrix_id', $talentmatrixId);

        $talentmatrixsGroupped = [];

        foreach ($talentmatrixQuery as $talentmatrix)
        {
            if (false === array_key_exists($talentmatrix['survey_question_id'], $surveyQuestions)) {
                continue;
            }
            $type = $surveyQuestions[$talentmatrix['survey_question_id']];
            $talentmatrixsGroupped[$talentmatrix['user_id']][$type][] = $talentmatrix['score'];
        }

        $talentmatrixs = [];
        foreach ($talentmatrixsGroupped as $userId => $types)
        {
            foreach ($types as $type => $scores)
            {
                $talentmatrixs[$userId][$type] = array_sum($scores) / count($scores);
            }
        }

        return $talentmatrixs;
    }

	public function getAllTotalScoreboards($talentmatrixId)
	{
		$allScores = $this->db->user_talentmatrix
			->select('user_id, ROUND(AVG(score), '. self::SCOREBOARD_PRECISION .') score')
			->where('talentmatrix_id', $talentmatrixId)
			->group('user_id');

		$results = [];
		foreach ($allScores as $score)
		{
			$results[] = $score['score'];
		}

		return $results;
	}

	private function isBossSurvey($surveyId, $talentMatrixId)
	{
		$bossQuestionId = $this->db->survey_question
				->where('question_type', 'boss')
				->and('talentmatrix_id', $talentMatrixId)
				->fetch('id');

		$isBoss = $this->db->survey_answer
				->where(['survey_id' => $surveyId, 'survey_question_id' => $bossQuestionId])
				->fetch('answer');

		return $isBoss ? true : false;
	}

	public function getSurveyCount($talentmatrixId, $userId)
	{
		return (int) $this->db->survey_answer
				->where(['user_id' => $userId, 'survey.talentmatrix.id'	=> $talentmatrixId,])
				->count('DISTINCT survey_id');
	}



	public function hasScoreboard($talentmatrixId, $userId)
	{
		return $this->getSurveyCount($talentmatrixId, $userId) >= self::MINIMUM_SURVEYS_FOR_SCOREBOARD ? true : false;
	}

	public function hasStoredScoreboard($talentmatrixId, $userId)
	{
		return $this->db->user_talentmatrix->where(['talentmatrix_id' => $talentmatrixId, 'user_id' => $userId])->fetch() ? true : false;
	}


	public function getQuestions($talentmatrixId)
	{
		$surveyQuestions = $this->db->survey_question->where([
			'talentmatrix_id' => $talentmatrixId,
			'question_type' => 'results'
		])
			->order('id');

		$questions = [];

		foreach ($surveyQuestions as $question)
		{
			$questions[$question['id']] = $question['question'];
		}
		
		return $questions;
	}

	public function getScores($talentmatrixId, $userId)
	{
		$surveyQuestions = $this->db->survey_question
			->where('question_type', ['results', 'values'])
			->where('talentmatrix_id', $talentmatrixId);

		$return = [];
		foreach($surveyQuestions as $surveyQuestion)
		{
			$surveyAnswers = $this->db->survey_answer->where([
				'user_id'                => $userId,
				'survey_question_id'     => $surveyQuestion['id'],
				'survey.talentmatrix.id' => $talentmatrixId,
			])->order('survey_question_id');

			$results = [];
			
			foreach ($surveyAnswers as $surveyAnswer)
			{
				$results[] = $surveyAnswer['answer'];
			}

			if (count($results))
			{
				$return[$surveyQuestion['id']] = array_sum($results) / count($results);
			}
		}
		
		return $return;
	}

	public function getTalentmatrix($talentmatrixId)
	{
		$talentmatrix = $this->db->talentmatrix->where('id', $talentmatrixId)->fetch();

		return $talentmatrix;
	}

	public function getAllTalentMatrixForUser($userId)
	{
		$talentmatrixs = [];
		foreach ($this->db->talentmatrix as $talentmatrix)
		{
			$talentmatrixs[$talentmatrix['id']] = $talentmatrix;
		}

		$userTalenMatrixs = $this->db
			->user_talentmatrix
            ->select('talentmatrix_id')
			->where('user_id', $userId)
			->group('talentmatrix_id');

		$results = [];

		foreach ($userTalenMatrixs as $userTalenMatrix)
		{
			$talentmatrix = $talentmatrixs[$userTalenMatrix['talentmatrix_id']];
			/** @var UserService $userService */
			$userService = $this->container->get('user');
			$userHash    = $userService->getUserHashFromId($userId);

			$results[] = [
				'name' => $talentmatrix['name'],
				'url'  => '/' . $talentmatrix['id'] . '/' . $userHash,
			];
		}

		return $results;

	}

}

