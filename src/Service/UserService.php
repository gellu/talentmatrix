<?php
/**
 * Author: gellu
 * Date: 09.04.2016 21:14
 */

namespace Service;

use Pimple\Container;

class UserService
{
	/** @var \NotORM */
	private $db;

	/** @var Container */
	private $container;

	/** @var string Salt for user hash */
	private $hashSalt = 'diou387fjkwehf8392yfhjesjkfhkhfk';

	public function __construct($container)
	{
		$this->container 	= $container;
		$this->db 			= $this->container->get('db');
	}

	public function getUserIdFromHash($hash)
	{
		return $this->encryptor('decrypt', $hash);
	}

	public function getUserHashFromId($userId)
	{
		return $this->encryptor('encrypt', $userId);
	}

	private function encryptor($action, $string)
	{
		$output = false;

		$encrypt_method = "AES-256-CBC";
		//pls set your unique hashing key
		$secret_key = $this->hashSalt;
		$secret_iv = $this->hashSalt . '123';

		// hash
		$key = hash('sha256', $secret_key);

		// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		$iv = substr(hash('sha256', $secret_iv), 0, 16);

		//do the encyption given text/string/number
		if( $action == 'encrypt' ) {
			$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
			$output = base64_encode($output);
		}
		else if( $action == 'decrypt' ){
			//decrypt the given text/string/number
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		}

		return $output;
	}
}

