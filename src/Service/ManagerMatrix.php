<?php
/**
 * @author  nediam
 * @date    15.11.2018 18:12
 */

namespace Service;


class ManagerMatrix implements MatrixInterface
{
	/**
	 * @var ScoreboardService
	 */
	private $scoreboard;

	/**
	 * LeaderMatrix constructor.
	 *
	 * @param ScoreboardService $scoreboardService
	 */
	public function __construct(ScoreboardService $scoreboardService)
	{
		$this->scoreboard = $scoreboardService;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getResults($talentmatrixId, $userId)
	{
		return [];
	}
}
