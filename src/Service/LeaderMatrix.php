<?php
/**
 * @author  nediam
 * @date    08.07.2016 08:26
 */

namespace Service;


class LeaderMatrix implements MatrixInterface
{
	/**
	 * @var ScoreboardService
	 */
	private $scoreboard;

	/**
	 * LeaderMatrix constructor.
	 *
	 * @param ScoreboardService $scoreboardService
	 */
	public function __construct(ScoreboardService $scoreboardService)
	{
		$this->scoreboard = $scoreboardService;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getResults($talentmatrixId, $userId)
	{
		$scores = $this->scoreboard->getScores($talentmatrixId, $userId);
		$allScoreboards = $this->scoreboard->getLeaderMatrixAllScoreboards($talentmatrixId);

		$allTotalScoreboards = $this->scoreboard->getAllTotalScoreboards($talentmatrixId);
		$totalAreas25 = $this->scoreboard->countQuartile2($allTotalScoreboards, 0.5);

		return [
            'scores'           => $scores,
            'survey_count'     => $this->scoreboard->getSurveyCount($talentmatrixId, $userId),
            'questions'        => $this->scoreboard->getQuestions($talentmatrixId),
            'score_avg'        => array_sum($scores) / count($scores),
            'scores_all'       => $allScoreboards,
            'areas_25'         => $this->scoreboard->countQuartiles($talentmatrixId, 0.5),
            'feedbacks'        => [
                'continue' => $this->scoreboard->getFeedback($talentmatrixId, $userId, ScoreboardService::SCOREBOARD_FEEDBACK_TYPE_CONTINUE),
                'stop'     => $this->scoreboard->getFeedback($talentmatrixId, $userId, ScoreboardService::SCOREBOARD_FEEDBACK_TYPE_STOP),
            ],
            'all_total_scores' => $allTotalScoreboards,
            'total_areas_25'   => $totalAreas25,
		];
	}
}
