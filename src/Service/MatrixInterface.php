<?php
/**
 * @author  nediam
 * @date    08.07.2016 08:30
 */

namespace Service;


interface MatrixInterface
{
	/**
	 * @param int $talentmatrixId
	 * @param int $userId
	 *
	 * @return array
	 */
	public function getResults($talentmatrixId, $userId);
}
