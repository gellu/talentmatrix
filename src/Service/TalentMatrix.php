<?php
/**
 * @author  nediam
 * @date    08.07.2016 08:25
 */

namespace Service;


class TalentMatrix implements MatrixInterface
{
	/**
	 * @var ScoreboardService
	 */
	private $scoreboard;

	/**
	 * LeaderMatrix constructor.
	 *
	 * @param ScoreboardService $scoreboard
	 *
	 */
	public function __construct(ScoreboardService $scoreboard)
	{
		$this->scoreboard = $scoreboard;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getResults($talentmatrixId, $userId)
	{
		$values = $this->scoreboard->getValues($talentmatrixId, $userId);
		$results = $this->scoreboard->getResults($talentmatrixId, $userId);
		
		$params = [
			'results'   		=> round($results['score'], ScoreboardService::SCOREBOARD_PRECISION-1, PHP_ROUND_HALF_UP),
			'values'    		=> round($values['score'], ScoreboardService::SCOREBOARD_PRECISION-1, PHP_ROUND_HALF_UP),
			'all_scoreboards'	=> $this->scoreboard->getAllScoreboards($talentmatrixId),
			'has_boss_feedback' => $results['has_boss'] ? true : false,
			'has_realisation'	=> $results['realisation'] > 0 ? true : false,
			'feedbacks' => [
				'continue'       => $this->scoreboard->getFeedback($talentmatrixId, $userId, ScoreboardService::SCOREBOARD_FEEDBACK_TYPE_CONTINUE),
				'stop'           => $this->scoreboard->getFeedback($talentmatrixId, $userId, ScoreboardService::SCOREBOARD_FEEDBACK_TYPE_STOP),
				'recommendation' => $this->scoreboard->getFeedback($talentmatrixId, $userId, null),
			],
			'area50x'		=> $this->scoreboard->getResultsQuartile75($talentmatrixId),
			'area50y'		=> $this->scoreboard->getValuesQuartile75($talentmatrixId),
			'area25x'		=> $this->scoreboard->getResultsMedian($talentmatrixId),
			'area25y'		=> $this->scoreboard->getValuesMedian($talentmatrixId),
			'questions'		=> $this->scoreboard->getQuestions($talentmatrixId),
		];

		return $params;
	}
}
