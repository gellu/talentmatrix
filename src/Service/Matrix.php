<?php
/**
 * @author  nediam
 * @date    08.07.2016 08:33
 */

namespace Service;


use Slim\Container;

class Matrix
{
	const TALENT_MATRIX = 1;
	const LEADER_MATRIX = 2;

	/**
	 * @var Container
	 */
	private $container;

	/**
	 * Matrix constructor.
	 *
	 * @param $container
	 */
	public function __construct(Container $container)
	{
		$this->container = $container;
	}

	/**
	 * @param string $type
	 *
	 * @return MatrixInterface
	 * @throws \Exception
	 */
	public function getMatrix($type)
	{
		$serviceId = sprintf('%sMatrix', $type);
		
		if (false === $this->container->has($serviceId))
		{
			throw new \Exception(sprintf('Given talentmatrix type not found "%s"', $type));
		}
		
		return $this->container->get($serviceId);
	}
}
