var ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports = {
	entry: {
		main: "./assets/css/main.js"
	},
	output: {
		path: __dirname + "/public/platform/css/",
		filename: "main.css"
	},
	module: {
		loaders: [
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract("css-loader!sass-loader")
			}
		]
	},
	plugins: [
		new ExtractTextPlugin("[name].css")
	]
};