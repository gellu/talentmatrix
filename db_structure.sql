/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table realisation
# ------------------------------------------------------------

CREATE TABLE `realisation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `talentmatrix_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `realisation` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `talentmatrix_id` (`talentmatrix_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table survey
# ------------------------------------------------------------

CREATE TABLE `survey` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `talentmatrix_id` int(11) unsigned DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `talentmatrix_id` (`talentmatrix_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table survey_answer
# ------------------------------------------------------------

CREATE TABLE `survey_answer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `survey_question_id` int(11) unsigned DEFAULT NULL,
  `answer` longtext,
  PRIMARY KEY (`id`),
  KEY `answer` (`survey_id`,`survey_question_id`),
  KEY `survey` (`survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table survey_question
# ------------------------------------------------------------

CREATE TABLE `survey_question` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `talentmatrix_id` int(11) DEFAULT NULL,
  `question_type` varchar(255) DEFAULT NULL,
  `question` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table survey_question_translation
# ------------------------------------------------------------

CREATE TABLE `survey_question_translation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `survey_question_id` int(11) unsigned DEFAULT NULL,
  `locale` varchar(5) DEFAULT NULL,
  `question` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table talentmatrix
# ------------------------------------------------------------

CREATE TABLE `talentmatrix` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from_period` date DEFAULT NULL,
  `to_period` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user
# ------------------------------------------------------------

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_department_id` int(11) unsigned DEFAULT NULL,
  `manager_user_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_department
# ------------------------------------------------------------

CREATE TABLE `user_department` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_talentmatrix
# ------------------------------------------------------------

CREATE TABLE `user_talentmatrix` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `talentmatrix_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `talentmatrix_results` float unsigned DEFAULT NULL,
  `talentmatrix_values` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
