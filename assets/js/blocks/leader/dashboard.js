var logo = document.querySelector('.logo');

var ctx = document.getElementById("resultsChart").getContext('2d');

var ids = ['resultsChart','turnoverChart','ovChart','teamdevChart','_270Chart','knowledgeChart'];
var detailedChartsIDs = ['roleModel','credibility','care','setGoals','partOfTheTeam','development','negotiation','communication','WWPP','execution','facilitation']

var chartNames = {
    roleModel:'Role Model',
    credibility:'Credibility',
    care:'Care',
    setGoals:'Set Goals',
    partOfTheTeam:'Part of the team',

    development: 'Development',
    negotiation: 'Negotiation',
    communication: 'Communication',
    WWPP: 'WWPP',
    execution: 'Execution',
    facilitation: 'Facilitation'
}

var chartsChild270 = document.getElementsByClassName('chart hidden _270');
var chartsChildKnowledge = document.getElementsByClassName('chart hidden _knowledge');

var chart = {};
for(var i = 0; i< ids.length; i++){
   chart[i] = document.getElementById(ids[i]);
}
var subCharts = [];
for(var i = 0; i< detailedChartsIDs.length; i++){
    subCharts[i] = document.getElementById(detailedChartsIDs[i]);
}

// get data from data-chart
// document.addEventListener('DOMContentLoaded', function() {
//     var userRating = document.querySelector('.chart-data');
//     var chartData = userRating.dataset.chart;
// });

var chartData = {
    results:[27,77,8],
    turnover:[89,20],
    teamdev:8,
    _270:{
        feedback:[8,4,3,2,7],
        self:[3,10,5,4,2],
        time:{
            roleModel:{
                "2017":[1,4,6,3],
                "2018":[7,4,6,null]
            },
            credibility:{
                "2017":[7,8,3,5],
                "2018":[6,3,7,2]
            },
            care:{
                "2017":[7,7,7,5],
                "2018":[6,3,7,2]
            },
            setGoals:{
                "2017":[0,0,0,0],
                "2018":[6,3,7,2]
            },
            partOfTheTeam:{
                "2017":[3,9,3,9],
                "2018":[6,3,7,2]
            },
        }
    },
    knowledge:{
        pre:[8,4,3,2,1,12],
        post:[3,10,5,4,2,11],
        time:{
            development:{
                "2017":[5,3,6,5],
                "2018":[7,4,6,8]
            },
            negotiation:{
                "2017":[9,9,4,9],
                "2018":[6,3,7,2]
            },
            communication:{
                "2017":[7,8,3,5],
                "2018":[6,3,7,2]
            },
            WWPP:{
                "2017":[7,8,3,5],
                "2018":[6,3,7,2]
            },
            execution:{
                "2017":[3,3,3,3],
                "2018":[2,2,2,2]
            },
            facilitation:{
                "2017":[7,0,0,0],
                "2018":[6,0,0,0]
            },
        }
    }
}

// colors
var colors = {
    $color1 : "rgba(0, 195, 165, .2)",
    $color1dark : "rgba(0, 195, 165, 1)",
    $color2 : "rgba(61, 131, 223, .2)",
    $color2dark : "rgba(61, 131, 223, 1)",
    $color3 : "rgba(255, 198, 0, .2)",
    $color3dark : "rgba(255, 198, 0, 1)",
    $color4 : "#F2EFEB",
    $color4dark : "#D3CFC9",
    $color5 : "#A7D8D0",
    $color6dark : "#8DCCC0"
}

var chartResults = new Chart(chart[0].getContext("2d"), {
    type: 'bar',
    data: {
        // labels: ["Red", "Blue", "Yellow"],
        datasets: [{
            label: 'results - individual (%)',
            yAxisID:'%',
            data: [chartData.results[0]],
            backgroundColor: [
                colors.$color1,
            ],
            borderColor: [
                colors.$color1dark,
            ],
            borderWidth: 1
        },{
            label: 'results - team (%)',
            yAxisID:'%',
            data: [chartData.results[1]],
            backgroundColor: [
                colors.$color2,
            ],
            borderColor: [
                colors.$color2dark,
            ],
            borderWidth: 1
        },{
            label: 'results - feedback (0-10)',
            yAxisID:'10',
            data: [chartData.results[2]],
            backgroundColor: [
                colors.$color3,
            ],
            borderColor: [
                colors.$color3dark,
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                id: '%',
                ticks: {
                    beginAtZero:true
                },
                position: 'left',
                scaleLabel: {
                    display: true,
                    labelString: "%",
                    fontFamily: "Montserrat",
                    fontColor: "black",
                  },
            },{
                id:'10',
                position: 'right',
                display: 'true',
                scaleLabel: {
                    display: true,
                    labelString: "0-10",
                    fontFamily: "Montserrat",
                    fontColor: "black",
                  },
            }]
        }
    }
});

var chartTurnover = new Chart(chart[1].getContext("2d"), {
    type: 'bar',
    data: {
        datasets: [{
            label: 'left people/all people in the team (%)',
            data: [chartData.turnover[0]],
            backgroundColor: [
                colors.$color1,
            ],
            borderColor: [
                colors.$color1dark,
            ],
            borderWidth: 1
        },{
            label: 'regretted loses score: (%)',
            data: [chartData.turnover[1]],
            backgroundColor: [
                colors.$color2,
            ],
            borderColor: [
                colors.$color2dark,
            ],
            borderWidth: 1
        },
         
        ]
    },
    options: {
        scales: {
            yAxes: [{
            
                ticks: {
                    beginAtZero:true,
                    min:0,
                    max:100
                },
            }]
        }
    }
});
var chartTeamDev = new Chart(chart[3].getContext("2d"), {
    type: 'bar',
    data: {
        datasets: [{
            data: [
                chartData.teamdev
            ],
            backgroundColor: [
                colors.$color1,
            ],
            borderColor: [
                colors.$color1dark,
            ],
            label: 'I learnt something new / developed my skills'
        }],
    },options: {
        scales: {
            yAxes: [{
            
                ticks: {
                    beginAtZero:true,
                    min:0,
                    max:10
                },
            }]
        }
    }
});
var chart270 = new Chart(chart[4].getContext("2d"), {
    type: 'radar',
    data: {
        labels: ['Role model','Credibility','Care','Set goals','Part of the team'],
        datasets: [{
            data: chartData._270.feedback,
            label:'Feedback',
            fill:'true',
            backgroundColor:colors.$color1,
            borderColor:colors.$color1dark,
            pointBackgroundColor:"rgb(54, 162, 235)",
            pointBorderColor:"#fff",
            pointHoverBackgroundColor:"#fff",
            pointHoverBorderColor:"rgb(54, 162, 235)"
        },{
            data: chartData._270.self,
            label:'Self',
            fill:'true',
            backgroundColor:colors.$color2,
            borderColor:colors.$color2dark,
            pointBackgroundColor:"rgb(255, 99, 132)",
            pointBorderColor:"#fff",
            pointHoverBackgroundColor:"#fff",
            pointHoverBorderColor:"rgb(255, 99, 132)"
        }
    ]},
    options:{
        elements:{
            line:{
                tension:0,
                borderWidth:3
            }
        },
        scale: {
            pointLabels: {
                fontSize:14
              },
        },
        legend:{
            display: true,
            labels:{
                fontSize:14,
                boxWidth: 30
            }
        }
    }
});
var chartKnowledge = new Chart(chart[5].getContext("2d"), {
    type: 'radar',
    data: {
        labels: ['development','negotiation','communication','WWPP','execution','facilitation'],
        datasets: [{
            data:chartData.knowledge.pre,
            label:'Pre',
            fill:'true',
            backgroundColor:colors.$color1,
            borderColor:colors.$color1dark,
            pointBackgroundColor:"rgb(54, 162, 235)",
            pointBorderColor:"#fff",
            pointHoverBackgroundColor:"#fff",
            pointHoverBorderColor:"rgb(54, 162, 235)"
        },{
            data: chartData.knowledge.post,
            label:'Post',
            fill:'true',
            backgroundColor:colors.$color2,
            borderColor:colors.$color2dark,
            pointBackgroundColor:"rgb(255, 99, 132)",
            pointBorderColor:"#fff",
            pointHoverBackgroundColor:"#fff",
            pointHoverBorderColor:"rgb(255, 99, 132)"
        }
    ]},
    options:{
        elements:{
            line:{
                tension:0,
                borderWidth:3
            }
        },
        scale: {
            pointLabels: {
                fontSize:14
              },
        }
    }
});


// time line charts 270

// for every area (IDs in DOM)

var quarters = ['Q1','Q2','Q3','Q4'];
var lineChartColors = Object.values(colors);

for(var i = 0; i < (Object.keys(chartNames).length ); i++){
    var labels = [];

    var chartAreaPrep = {};

    let dataName = subCharts[i].getAttribute('id');

    chartAreaPrep.type = 'line';
    chartAreaPrep.data = {};
    chartAreaPrep.data.datasets = [{}];
    chartAreaPrep.data.datasets[0].data = [];
    chartAreaPrep.data.datasets[0].label = chartNames[dataName];
    chartAreaPrep.data.datasets[0].backgroundColor = lineChartColors[i-5];
    chartAreaPrep.data.datasets[0].borderColor = lineChartColors[i-2];
    chartAreaPrep.options = {
        responsive: true,
        maintainAspectRatio: false,
    }

    // quarterCounter that helps making data indexes better
    var quarterCounter = 0;

    // _270 charts
    if(i<5){
        Object.entries(chartData._270.time[dataName]).forEach(
            ([year,value]) => {

                value.forEach((val,index) => {
                    labels.push(year+'/'+quarters[index]);

                    if(val!==null){
                        let data = {x:quarterCounter,y:val}
                        chartAreaPrep.data.datasets[0].data.push(data);
                    }
                    quarterCounter ++;
                });
            }
        );
    } else {
        // _Knowledge charts
        Object.entries(chartData.knowledge.time[dataName]).forEach(
            ([year,value]) => {

                value.forEach((val,index) => {
                    labels.push(year+'/'+quarters[index]);

                    if(val!==null){
                        let data = {x:quarterCounter,y:val}
                        chartAreaPrep.data.datasets[0].data.push(data);
                    }
                    quarterCounter ++;
                });
            }
        );
    }
    console.log(subCharts[i]);
    chartAreaPrep.data.labels = labels;
    var chartArea = new Chart(subCharts[i].getContext("2d"),chartAreaPrep);

}

function resize(el){
    var charts = document.getElementsByClassName('chart');
    var leftDistance = el.getBoundingClientRect().left;
    var containerWidth = document.getElementsByClassName('dashboard__wrapper')[0].offsetWidth;

    if(el.classList.contains('clicked')){
        el.classList.remove('clicked','leftCol','rightCol');
    } else {
        for(var i = 0; i<charts.length;i++){
            charts[i].classList.remove('clicked');
        };

        if(leftDistance < (containerWidth/4)){
            el.classList.add('clicked','leftCol');
        } else if(leftDistance > (containerWidth*(2/5))){
            el.classList.add('clicked','rightCol');
        } else{
            el.classList.add('clicked');
        }
    }
}

function showMore(e,chartType){
    e.classList.toggle('hide');
    if(e.classList.contains('hide')){
        e.innerHTML= 'Show less';
    } else {
        e.innerHTML= 'Show details';
    }
    if(chartType === '270'){
        for (var i = 0; i < chartsChild270.length; i++) {
            chartsChild270[i].classList.toggle('show');
        }
    } else if(chartType === 'knowledge'){
        for (var i = 0; i < chartsChildKnowledge.length; i++) {
            chartsChildKnowledge[i].classList.toggle('show');
        }
    }
}

// device width
var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;

function scrollFunction() {
    
    var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
	
	if(scrollTop>70){
		logo.classList.add('scrolled');
    } else{
		if(logo.classList.contains('scrolled')){
			logo.classList.remove('scrolled');
		}
	}
}


if(width>728){
    setTimeout(function(){
        window.onscroll = scrollFunction;
    },200);
} 

function showMenu(){
    var hamburger = document.getElementById('hamburger');
    var navbar = document.getElementById('navbar');

    navbar.classList.toggle('show');
    hamburger.classList.toggle('clicked')
}

